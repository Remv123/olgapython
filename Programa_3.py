import math
l1=[1,2,3,4,5,6,7,8,9,10]
lambda_suma=lambda x, y: x+y
par= lambda x: x%2==0
impar=lambda x: x%2!= 0 
multiplo3=lambda x: x%3==0
l2=list(map(lambda_suma, filter(par,l1), filter(impar,l1)))
print(l2)
l3=list(filter(multiplo3,l2))
print(l3)

